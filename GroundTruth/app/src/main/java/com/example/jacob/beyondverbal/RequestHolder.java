package com.example.jacob.beyondverbal;

import org.apache.http.Header;
import org.apache.http.HttpEntity;

public class RequestHolder {
    public String url;
    public Header header;
    public HttpEntity httpen;
}
