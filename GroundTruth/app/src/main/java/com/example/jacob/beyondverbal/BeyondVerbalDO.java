package com.example.jacob.beyondverbal;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

@DynamoDBTable(tableName = "groundtruth-mobilehub-1292017806-BeyondVerbal")

public class BeyondVerbalDO {
    private String _recordingID;
    private String _username;
    private String _arousalgroup;
    private Double _arousalscore;
    private Double _arousalvalue;
    private String _tempergroup;
    private Double _temperscore;
    private Double _tempervalue;
    private Long _timestamp;
    private Double _valenceScore;
    private String _valencegroup;
    private Double _valencevalue;
    private String _phoneID;

    @DynamoDBHashKey(attributeName = "recordingID")
    @DynamoDBAttribute(attributeName = "recordingID")
    public String getRecordingID() {
        return _recordingID;
    }

    public void setRecordingID(final String _recordingID) {
        this._recordingID = _recordingID;
    }
    @DynamoDBAttribute(attributeName = "Username")
    public String getUsername() {
        return _username;
    }

    public void setUsername(final String _username) {
        this._username = _username;
    }
    @DynamoDBAttribute(attributeName = "arousalgroup")
    public String getArousalgroup() {
        return _arousalgroup;
    }

    public void setArousalgroup(final String _arousalgroup) {
        this._arousalgroup = _arousalgroup;
    }
    @DynamoDBAttribute(attributeName = "arousalscore")
    public Double getArousalscore() {
        return _arousalscore;
    }

    public void setArousalscore(final Double _arousalscore) {
        this._arousalscore = _arousalscore;
    }
    @DynamoDBAttribute(attributeName = "arousalvalue")
    public Double getArousalvalue() {
        return _arousalvalue;
    }

    public void setArousalvalue(final Double _arousalvalue) {
        this._arousalvalue = _arousalvalue;
    }
    @DynamoDBAttribute(attributeName = "tempergroup")
    public String getTempergroup() {
        return _tempergroup;
    }

    public void setTempergroup(final String _tempergroup) {
        this._tempergroup = _tempergroup;
    }
    @DynamoDBAttribute(attributeName = "temperscore")
    public Double getTemperscore() {
        return _temperscore;
    }

    public void setTemperscore(final Double _temperscore) {
        this._temperscore = _temperscore;
    }
    @DynamoDBAttribute(attributeName = "tempervalue")
    public Double getTempervalue() {
        return _tempervalue;
    }

    public void setTempervalue(final Double _tempervalue) {
        this._tempervalue = _tempervalue;
    }

    @DynamoDBAttribute(attributeName = "timestamp")
    public Long getTimestamp() {
        return _timestamp;
    }
    public void setTimestamp(final Long _timestamp) {
        this._timestamp = _timestamp;
    }

    @DynamoDBAttribute(attributeName = "valenceScore")
    public Double getValenceScore() {
        return _valenceScore;
    }

    public void setValenceScore(final Double _valenceScore) {
        this._valenceScore = _valenceScore;
    }
    @DynamoDBAttribute(attributeName = "valencegroup")
    public String getValencegroup() {
        return _valencegroup;
    }

    public void setValencegroup(final String _valencegroup) {
        this._valencegroup = _valencegroup;
    }
    @DynamoDBAttribute(attributeName = "valencevalue")
    public Double getValencevalue() {
        return _valencevalue;
    }

    public void setValencevalue(final Double _valencevalue) {
        this._valencevalue = _valencevalue;
    }


    @DynamoDBAttribute(attributeName = "phoneID")
    public String getphoneID() {
        return _phoneID;
    }

    public void setphoneID(final String _phoneID) {
        this._phoneID = _phoneID;
    }
}
