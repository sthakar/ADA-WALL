package com.example.jacob.groundtruth;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBIndexHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBIndexRangeKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBRangeKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

import java.util.List;
import java.util.Map;
import java.util.Set;

@DynamoDBTable(tableName = "adaemo-mobilehub-1828478408-GroundTruthTable")

public class GroundTruthTableDO {
    private String _userId;
    private Double _angry;
    private Double _happy;
    private Double _other;
    private Double _sad;
    private String _timestamp;
    private String _username;
    private String _phoneID;



    @DynamoDBHashKey(attributeName = "UserId ")
    @DynamoDBIndexHashKey(attributeName = "UserId ", globalSecondaryIndexName = "Index")
    public String getUserId() {
        return _userId;
    }
    public void setUserId(final String _userId) {
        this._userId = _userId;
    }

    @DynamoDBAttribute(attributeName = "Angry")
    public Double getAngry() {
        return _angry;
    }
    public void setAngry(final Double _angry) {
        this._angry = _angry;
    }

    @DynamoDBAttribute(attributeName = "Happy")
    public Double getHappy() {
        return _happy;
    }
    public void setHappy(final Double _happy) {
        this._happy = _happy;
    }

    @DynamoDBAttribute(attributeName = "Other")
    public Double getOther() {
        return _other;
    }
    public void setOther(final Double _other) {
        this._other = _other;
    }

    @DynamoDBAttribute(attributeName = "Sad")
    public Double getSad() {
        return _sad;
    }
    public void setSad(final Double _sad) {
        this._sad = _sad;
    }

    @DynamoDBAttribute(attributeName = "Timestamp")
    public String getTimestamp() {
        return _timestamp;
    }
    public void setTimestamp(final String _timestamp) {
        this._timestamp = _timestamp;
    }

    @DynamoDBAttribute(attributeName = "username")
    public String getUsername() {
        return _username;
    }
    public void setUsername(final String _username) {
        this._username = _username;
    }

    @DynamoDBAttribute(attributeName = "phoneID")
    public String getPhoneID() {
        return _phoneID;
    }
    public void setPhoneID(final String _phoneID) {
        this._phoneID = _phoneID;
    }

}
