package com.example.jacob.facerecognition;


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

@DynamoDBTable(tableName = "groundtruth-mobilehub-1292017806-FaceRecognition")

public class FaceRecognitionDO {
    private String _userId;
    private Double _anger;
    private Double _contempt;
    private Double _fear;
    private Double _joy;
    private Double _sadness;
    private Double _smile;
    private Double _surprise;
    private String _username;
    private Long _timestamp;
    private String _phoneID;


    @DynamoDBHashKey(attributeName = "userId")
    @DynamoDBAttribute(attributeName = "userId")
    public String getUserId() {
        return _userId;
    }

    public void setUserId(final String _userId) {
        this._userId = _userId;
    }
    @DynamoDBAttribute(attributeName = "Anger")
    public Double getAnger() {
        return _anger;
    }

    public void setAnger(final Double _anger) {
        this._anger = _anger;
    }
    @DynamoDBAttribute(attributeName = "Contempt")
    public Double getContempt() {
        return _contempt;
    }

    public void setContempt(final Double _contempt) {
        this._contempt = _contempt;
    }
    @DynamoDBAttribute(attributeName = "Fear")
    public Double getFear() {
        return _fear;
    }

    public void setFear(final Double _fear) {
        this._fear = _fear;
    }
    @DynamoDBAttribute(attributeName = "Joy")
    public Double getJoy() {
        return _joy;
    }

    public void setJoy(final Double _joy) {
        this._joy = _joy;
    }
    @DynamoDBAttribute(attributeName = "Sadness")
    public Double getSadness() {
        return _sadness;
    }

    public void setSadness(final Double _sadness) {
        this._sadness = _sadness;
    }
    @DynamoDBAttribute(attributeName = "Smile")
    public Double getSmile() {
        return _smile;
    }

    public void setSmile(final Double _smile) {
        this._smile = _smile;
    }
    @DynamoDBAttribute(attributeName = "Surprise")
    public Double getSurprise() {
        return _surprise;
    }

    public void setSurprise(final Double _surprise) {
        this._surprise = _surprise;
    }
    @DynamoDBAttribute(attributeName = "Username")
    public String getUsername() {
        return _username;
    }

    public void setUsername(final String _username) {
        this._username = _username;
    }
    @DynamoDBAttribute(attributeName = "Timestamp")
    public Long getTimestamp() {
        return _timestamp;
    }

    public void setTimestamp(final Long _timestamp) {
        this._timestamp = _timestamp;
    }


    @DynamoDBAttribute(attributeName = "phoneID")
    public String getphoneID() {
        return _phoneID;
    }

    public void setphoneID(final String _phoneID) {
        this._phoneID = _phoneID;
    }

}