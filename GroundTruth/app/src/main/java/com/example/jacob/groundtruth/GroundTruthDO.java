package com.example.jacob.groundtruth;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

@DynamoDBTable(tableName = "groundtruth-mobilehub-1292017806-GroundTruth")

public class GroundTruthDO {
    private String _userId;
    private Double _angry;
    private Double _disgust;
    private Double _happy;
    private Double _sad;
    private Double _surprise;
    private Double _fear;
    private Long _timestamp;
    private String _username;
    private String _phoneID;

    @DynamoDBHashKey(attributeName = "userId")
    @DynamoDBAttribute(attributeName = "userId")
    public String getUserId() {
        return _userId;
    }
    public void setUserId(final String _userId) {
        this._userId = _userId;
    }

    @DynamoDBAttribute(attributeName = "Angry")
    public Double getAngry() {
        return _angry;
    }
    public void setAngry(final Double _angry) {
        this._angry = _angry;
    }

    @DynamoDBAttribute(attributeName = "Disgust")
    public Double getDisgust() {
        return _disgust;
    }
    public void setDisgust(final Double _disgust) {
        this._disgust = _disgust;
    }

    @DynamoDBAttribute(attributeName = "Happy")
    public Double getHappy() {
        return _happy;
    }
    public void setHappy(final Double _happy) {
        this._happy = _happy;
    }

    @DynamoDBAttribute(attributeName = "Sad")
    public Double getSad() {
        return _sad;
    }
    public void setSad(final Double _sad) {
        this._sad = _sad;
    }

    @DynamoDBAttribute(attributeName = "Surprise")
    public Double getSurprise() {
        return _surprise;
    }
    public void setSurprise(final Double _surprise) {
        this._surprise = _surprise;
    }

    @DynamoDBAttribute(attributeName = "Fear")
    public Double getFear() {
        return _fear;
    }
    public void setFear(final Double _fear) {
        this._surprise = _fear;
    }

    @DynamoDBAttribute(attributeName = "Timestamp")
    public Long getTimestamp() { return _timestamp; }
    public void setTimestamp(final Long _timestamp) {
        this._timestamp = _timestamp;
    }

    //username
    @DynamoDBAttribute(attributeName = "username")
    public String getUsername() {
        return _username;
    }
    public void setUsername(final String _username) {
        this._username = _username;
    }

    //phone id
    @DynamoDBAttribute(attributeName = "phoneID")
    public String getPhoneID() {
        return _phoneID;
    }
    public void setPhoneID(final String _phoneID) {
        this._phoneID = _phoneID;
    }


}
