package com.example.jacob.beyondverbal;

/**
 * Created by yang on 2/14/18.
 */

public class ResponseHolder {
    public int actionId;
    public String content;
    public int responseCode;
    public String responseString;
}