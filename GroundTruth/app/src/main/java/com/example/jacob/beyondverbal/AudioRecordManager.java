package com.example.jacob.beyondverbal;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.Process;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.media.MediaRecorder.AudioSource.MIC;

/**
 * Created by yang on 2/14/18.
 */

public class AudioRecordManager {

    public static final String TAG = "AudioRecordManager";
    private AudioRecord mRecorder;
    private DataOutputStream dos;
    private Thread recordThread;
    private boolean isStart = false;
    private static AudioRecordManager mInstance;
    private  int bufferSize;
    private static int audioRate = 8000;
    //wav dir
    private String outFileName;
    //pcm dir
    private String inFileName;
    private File pcmFile;
    private File wavFile;

    public AudioRecordManager() {
        bufferSize = AudioRecord.getMinBufferSize(audioRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        mRecorder = new AudioRecord(MIC, audioRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize * 2);
    }

    public void createFile(String basePath){
        outFileName = basePath+"/voice.wav";
        inFileName = basePath+"/voice.pcm";
        File baseFile = new File(basePath);
        if(!baseFile.exists())
            baseFile.mkdirs();


        pcmFile = new File(inFileName);
        wavFile = new File(outFileName);

        if(pcmFile.exists())
            pcmFile.delete();

        if(wavFile.exists())
            wavFile.delete();


        try{
            pcmFile.createNewFile();
            wavFile.createNewFile();
        }catch(IOException e){
            e.printStackTrace();
        }
    }


    public static AudioRecordManager getInstance() {
        if (mInstance == null) {
            synchronized (AudioRecordManager.class) {
                if (mInstance == null) {
                    mInstance = new AudioRecordManager();
                }
            }
        }
        return mInstance;
    }

    private void destroyThread() {
        try{
            isStart = false;
            if(recordThread != null && Thread.State.RUNNABLE == recordThread.getState()){
                Thread.sleep(500);
                recordThread.interrupt();
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            recordThread = null;
        }
    }

    private void startThread(){
        destroyThread();
        isStart = true;
        if (recordThread == null){
            recordThread = new Thread(recordRunnable);
            recordThread.start();
        }else{
            Log.e(TAG, "Not started!");
        }
    }

    Runnable recordRunnable = new Runnable() {
        @Override
        public void run() {
            try{
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
                int bytesRecord;
                byte[] buffer = new byte[bufferSize];
                if(mRecorder.getState() != AudioRecord.STATE_INITIALIZED) {
                    stopRecord();
                    return;
                }
                mRecorder.startRecording();
                //writeToFileHead();
                while (isStart) {
                    if (null != mRecorder) {
                        bytesRecord = mRecorder.read(buffer, 0, buffer.length);
                        if (bytesRecord == AudioRecord.ERROR_INVALID_OPERATION || bytesRecord == AudioRecord.ERROR_BAD_VALUE) {
                            continue;
                        }
                        if (bytesRecord != 0 && bytesRecord != -1) {
                            // TO-DO add effects here if needed
                            dos.write(buffer, 0, bytesRecord);
                        } else {
                            break;
                        }
                    }else{
                        Log.e(TAG, "Not started!");
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    private void setPath(String path) throws Exception {
        File file = new File(path + "/raw.pcm");
        File wavFile = new File(path + "/raw.wav");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        //dos = new DataOutputStream(new FileOutputStream(file, true));
    }

    public void startRecord(String basePath) {
        try {
            createFile(basePath);
            //setPath(path);
            dos = new DataOutputStream(new FileOutputStream(pcmFile, true));
            startThread();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopRecord() {
        try {
            destroyThread();
            if (mRecorder != null) {
                if (mRecorder.getState() == AudioRecord.STATE_INITIALIZED) {
                    mRecorder.stop();
                }
                if (mRecorder != null) {
                    mRecorder.release();
                }
                mRecorder = new AudioRecord(MIC, audioRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize * 2);
            }
            if (dos != null) {
                dos.flush();
                dos.close();

                //convertWaveFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void WriteWaveFileHeader(FileOutputStream out, long totalAudioLen, long totalDataLen, long longSampleRate,
                                     int channels, long byteRate) throws IOException {
        byte[] header = new byte[44];
        header[0] = 'R'; // RIFF
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);//数据大小
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';//WAVE
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        //FMT Chunk
        header[12] = 'f'; // 'fmt '
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';//过渡字节
        //data size
        header[16] = 16; // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        //Encoding method PCM is using 10H
        header[20] = 1; // format = 1
        header[21] = 0;
        // channels_width
        header[22] = (byte) channels;
        header[23] = 0;
        //Sampling rate
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        //audio transmitting rate = Sampling rate * channel_width * sampling depth/8
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        // Buffer size，buffer_size = channels_width * sampling_bits
        header[32] = (byte) (1 * 16 / 8);
        header[33] = 0;
        //sample_width
        header[34] = 16;
        header[35] = 0;
        //Data chunk
        header[36] = 'd';//data
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);
        out.write(header, 0, 44);

    }

    public void convertWaveFile() {
        FileInputStream in = null;
        FileOutputStream out = null;
        long totalAudioLen = 0;
        long totalDataLen = totalAudioLen + 36;
        long longSampleRate = AudioRecordManager.audioRate;
        int channels = 1;
        long byteRate = 16 * AudioRecordManager.audioRate * channels / 8;
        byte[] data = new byte[bufferSize];
        try {
            in = new FileInputStream(inFileName);
            out = new FileOutputStream(outFileName);
            totalAudioLen = in.getChannel().size();
            //not including RIFF and WAV
            totalDataLen = totalAudioLen + 36;
            WriteWaveFileHeader(out, totalAudioLen, totalDataLen, longSampleRate, channels, byteRate);
            while (in.read(data) != -1) {
                out.write(data);
            }
            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
