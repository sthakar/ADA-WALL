package com.example.jacob.groundtruth;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentCallbacks;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.provider.Settings;

import com.affectiva.android.affdex.sdk.Frame;
import com.affectiva.android.affdex.sdk.detector.CameraDetector;
import com.affectiva.android.affdex.sdk.detector.Detector;
import com.affectiva.android.affdex.sdk.detector.Face;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.AWSStartupHandler;
import com.amazonaws.mobile.client.AWSStartupResult;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.batch.android.Batch;
import com.batch.android.BatchActionActivity;
import com.batch.android.BatchActionService;
import com.batch.android.BatchActivityLifecycleHelper;
import com.batch.android.Config;
import com.example.jacob.beyondverbal.AudioRecordManager;
import com.example.jacob.beyondverbal.BeyondVerbalDO;
import com.example.jacob.beyondverbal.HttpActivity;
import com.example.jacob.beyondverbal.ResponseHolder;
import com.example.jacob.facerecognition.FaceRecognitionDO;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;


public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, Detector.ImageListener, CameraDetector.CameraEventListener {
    // for every bar add a:
    // private static SeekBar (name);
    private static SeekBar happy_bar;
    private static SeekBar sad_bar;
    private static SeekBar angry_bar;
    private static SeekBar other_bar;

    private static RadioGroup radio_group;
    private static RadioButton happy_button;
    private static RadioButton angry_button;
    private static RadioButton sad_button;
    private static RadioButton surprise_button;
    private static RadioButton disgust_button;
    private static RadioButton fear_button;

    Button button;
    private static final String TAG = "MainActivity";
    // Declare a DynamoDBMapper object
    DynamoDBMapper dynamoDBMapper;


    // objects we need for beyond verbal
    private static final String RECORDING_URL = "https://apiv4.beyondverbal.com/v4/recording/";

    private static final String Auth_URL = "https://token.beyondverbal.com/token";


    private static final String APIKey = "dbb91b90-7775-4f91-b539-f707e20c40a1\n";
    private Header access_token;
    private String recordingid;
    private String basePath;
    private Button upstreamButton;
    private Button sendFileButton;
    private Button recordButton;
    private TextView statusContent;
    private TextView responseContentTextView;
    private TextView txtWait;
    private ProgressDialog progressDialog;
    private ResponseHolder responseHolder;
    private AudioRecordManager audioRecordManager;
    private View mLayout;
    private ProgressBar audioProgressBar;


    // objects we need for Face Recognition

    final String LOG_TAG = "CameraDetectorDemo";

    Button startSDKButton;
    Button surfaceViewVisibilityButton;
    TextView smileTextView;
    TextView ageTextView;
    TextView ethnicityTextView;
    ToggleButton toggleButton;

    // humberto: added textview variables for emotions
    TextView joyTextView;
    TextView angerTextView;
    TextView disgustTextView;
    TextView contemptTextView;
    TextView fearTextView;
    TextView sadnessTextView;
    TextView surpriseTextView;
    //////////////////////////////////////////////////

    // adding number and string values for these emotions

    //username textview
    TextView usernameTextView;

    float smileface;
    float joyface;
    float angerface;
    float disgustface;
    float contemptface;
    float fearface;
    float sadnessface;
    float surpriseface;
    boolean faceDetect = false;
    boolean voiceDetect = false;
    long currentUnixTime = (long) 0;

    SurfaceView cameraPreview;

    boolean isCameraBack = false;
    boolean isSDKStarted = false;
    boolean labelSet = false;
    boolean verbalSet = false;
    boolean facialSet = false;

    RelativeLayout mainLayout;

    CameraDetector detector;

    int previewWidth = 0;
    int previewHeight = 0;
    private final int MY_PERMISSIONS_CAMERA = 1;

    private void setRadioListeners(){
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            //allow label to be sent upstream only if one is selected
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                button.setEnabled(true);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        Batch.setConfig(new Config("DEV5B918C7AC73745EB17D9EC04AA6")); // development
        //registerComponentCallbacks(new BatchActivityLifecycleHelper());
        //registerComponentCallbacks((ComponentCallbacks) new BatchActivityLifecycleHelper());

        setContentView(R.layout.activity_main);
        // add another assignment for every bar
        /*
        happy_bar = findViewById(R.id.happyBar);
        sad_bar = findViewById(R.id.sadBar);
        angry_bar = findViewById(R.id.angryBar);
        other_bar = findViewById(R.id.otherBar);
        */
        audioProgressBar = findViewById(R.id.audioProgressBar);
        button = findViewById(R.id.button_send);
        button.setEnabled(false);
        button.setOnClickListener(new send_emotion());


        radio_group = findViewById(R.id.radiogroup);
        happy_button = findViewById(R.id.Happyradio);
        sad_button = findViewById(R.id.Sadradio);
        angry_button = findViewById(R.id.angryradio);
        disgust_button = findViewById(R.id.Disgustedradio);
        surprise_button = findViewById(R.id.Surprisedradio);
        fear_button = findViewById(R.id.Fearradio);
        usernameTextView = findViewById(R.id.usernameTextView);
        usernameTextView.setEnabled(false);



        // this is to initialize my AWS Client
        AWSMobileClient.getInstance().initialize(this, new AWSStartupHandler() {
            @Override
            public void onComplete(AWSStartupResult awsStartupResult) {
                Log.d("YourMainActivity", "AWSMobileClient is instantiated and you are connected to AWS!");
            }
        }).execute();

        // Instantiate a AmazonDynamoDBMapperClient
        AmazonDynamoDBClient dynamoDBClient = new AmazonDynamoDBClient(AWSMobileClient.getInstance().getCredentialsProvider());
        this.dynamoDBMapper = DynamoDBMapper.builder()
                .dynamoDBClient(dynamoDBClient)
                .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                .build();

        // This is to initialize beyond verbal

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);

        }

        responseHolder = new ResponseHolder();
        audioRecordManager = new AudioRecordManager();
        //basePath = Environment.DIRECTORY_DOCUMENTS;
        basePath = Environment.getExternalStorageDirectory().getPath();

        recordButton = findViewById(R.id.record_switch_button);
        recordButton.setOnClickListener(new send_verbal());

        initViews();

        // for camera permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
            }
        }


        toggleButton = findViewById(R.id.front_back_toggle_button);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCameraBack = isChecked;
                switchCamera(isCameraBack ? CameraDetector.CameraType.CAMERA_BACK : CameraDetector.CameraType.CAMERA_FRONT);
            }
        });

        startSDKButton = findViewById(R.id.sdk_start_button);
        startSDKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSDKStarted) {
                    isSDKStarted = false;
                    stopDetector();
                    startSDKButton.setText("Start Camera");
                } else {
                    isSDKStarted = true;
                    startDetector();
                    startSDKButton.setText("Stop Camera");
                }
            }
        });
        //startSDKButton.setText("Start Camera");

        //We create a custom SurfaceView that resizes itself to match the aspect ratio of the incoming camera frames
        mainLayout = findViewById(R.id.theLayout);
        cameraPreview = new SurfaceView(this) {
            @Override
            public void onMeasure(int widthSpec, int heightSpec) {
                int measureWidth = MeasureSpec.getSize(widthSpec);
                int measureHeight = MeasureSpec.getSize(heightSpec);
                int width;
                int height;
                if (previewHeight == 0 || previewWidth == 0) {
                    width = measureWidth;
                    height = measureHeight;
                } else {
                    float viewAspectRatio = (float) measureWidth / measureHeight;
                    float cameraPreviewAspectRatio = (float) previewWidth / previewHeight;

                    if (cameraPreviewAspectRatio > viewAspectRatio) {
                        width = measureWidth;
                        height = (int) (measureWidth / cameraPreviewAspectRatio);
                    } else {
                        width = (int) (measureHeight * cameraPreviewAspectRatio);
                        height = measureHeight;
                    }
                }

                // Humberto: commented this out, uncomment and comment out line below to show camera preview
                // comment this out if you don't want camera preview and uncomment line below
                //setMeasuredDimension(width,height);

                // Humberto: set phone screen width and height to 1, to minimize camera preview
                // comment this out if you want to see camera preview and uncomment line above
                //setMeasuredDimension(1,1);
                setMeasuredDimension(300, 400);
            }
        };
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        cameraPreview.setLayoutParams(params);
        mainLayout.addView(cameraPreview, 0);

        surfaceViewVisibilityButton = findViewById(R.id.surfaceview_visibility_button);
        surfaceViewVisibilityButton.setVisibility(View.VISIBLE);
        surfaceViewVisibilityButton.setText("HIDE CAMERA");
        surfaceViewVisibilityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cameraPreview.getVisibility() == View.VISIBLE) {
                    cameraPreview.setVisibility(View.INVISIBLE);
                    surfaceViewVisibilityButton.setText("SHOW CAMERA");
                } else {
                    cameraPreview.setVisibility(View.VISIBLE);
                    surfaceViewVisibilityButton.setText("HIDE CAMERA");
                }
            }
        });

        // Humberto: To hide SurfaceView on Screen
        // comment this out to show camera preview
        //cameraPreview.setAlpha(0);
        cameraPreview.setY(-150);
        cameraPreview.setX(100);
        /////////////////////////////////////////

        detector = new CameraDetector(this, CameraDetector.CameraType.CAMERA_FRONT, cameraPreview);
        detector.setDetectSmile(true);
        detector.setDetectAge(true);
        detector.setDetectEthnicity(true);
        detector.setImageListener(this);
        detector.setOnCameraEventListener(this);

        // humberto: set affectiva API emotion detection metrics to true, to measure joy, anger, and disgust
        detector.setDetectAllEmotions(true);
        ///////////////////////////////////////////////////////////////

    }

    public void onRadioButtonClicked(View view) {
        button.setEnabled(true);
    }


    class send_emotion implements View.OnClickListener {
        @Override
        public void onClick(View v) { // TODO Auto-generated method stub
            // Where seek_bar is whatever barname to get the integer
            //seek_bar.getProgress();
            // need to add stuff to send info to the server when you hit the button
            String id = UUID.randomUUID().toString();
            int h = 0;
            if (happy_button.isChecked())
                h = 1;
            int a = 0;
            if (angry_button.isChecked())
                a = 1;
            Log.d("Happy Value", Integer.toString(h));
            //int a = angry_bar.getProgress();
            int s = 0;
            if (sad_button.isChecked())
                s = 1;
            int su = 0;
            if (surprise_button.isChecked())
                su = 1;
            int di = 0;
            if (disgust_button.isChecked())
                di = 1;
            int df = 0;
            if(fear_button.isChecked())
                df = 1;
            //int s = sad_bar.getProgress();
            //int o = other_bar.getProgress();
            createEmotion(id, (double) h, (double) a, (double) s, (double) su, (double) di, (double) df, (String) usernameTextView.getText().toString());
            if (faceDetect) {
                createFaceEmotion(id);
            }
            if(voiceDetect){
                goStream();
            }

            // we also send face recognition in send_emotion

        }
    }

    class send_verbal implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            audioProgressBar.setVisibility(View.VISIBLE);
            audioRecordManager.startRecord(basePath);
            final int timer = 15;
            recordButton.setText("RECORDING " + timer);
            recordButton.setEnabled(false);
            Handler handler = new Handler();
            Handler handler1 = new Handler();
            checkPermission();
            final Runnable r = new Runnable() {
                @Override
                public void run() {
                    audioRecordManager.stopRecord();

                    audioRecordManager.convertWaveFile();
                    //button.setText("START RECORD");
                    recordButton.setEnabled(true);
                    audioProgressBar.setProgress(0);
                    audioProgressBar.setVisibility(View.INVISIBLE);
                    File f = new File(basePath + "/voice.wav");
                    if (f.exists())
                        Toast.makeText(getApplicationContext(), "wav saved with size + " + f.length(), Toast.LENGTH_LONG).show();
                }
            };

            handler.postDelayed(r, timer * 1000);
            Thread t = new Thread() {

                @Override
                public void run() {
                    try {
                        int time1 = timer;
                        while (time1 >= 0) {


                            final int time2 = --time1;
                            runOnUiThread(new Runnable() {
                                int time = time2;

                                @Override
                                public void run() {
                                    // update TextView here
                                    String text = time >= 0 ? "RECORDING " + time : "START RECORD";
                                    recordButton.setText(text);
                                    //progress bar
                                    //no animation
                                    audioProgressBar.setProgress(timer - time);

                                    //*******uncomment below to test animation************
                                    //audioProgressBar.setProgress(time, true);
                                    //
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            if (time1 >= 0)
                                Thread.sleep(1000);
                        }
                        voiceDetect = true;
                        analyzeRecording();
                    } catch (InterruptedException e) {
                    }
                }
            };

            t.start();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


    public class Notify extends FirebaseInstanceIdService {
        @Override
        public void onTokenRefresh() {
            // Get updated InstanceID token.
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "Refreshed token: " + refreshedToken);

            // If you want to send messages to this application instance or
            // manage this apps subscriptions on the server side, send the
            // Instance ID token to your app server.
            // sendRegistrationToServer(refreshedToken);
        }
    }

    public void createEmotion(String id, double h, double a, double s, double su, double di, double df, String username) {
        final GroundTruthDO truthDo = new GroundTruthDO();

        truthDo.setUsername(username);
        truthDo.setPhoneID(Settings.Secure.getString(MainActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID));
        truthDo.setUserId(id);
        truthDo.setHappy(h);
        truthDo.setAngry(a);
        truthDo.setSad(s);
        truthDo.setSurprise(su);
        truthDo.setDisgust(di);
        truthDo.setFear(df);
        /*
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        truthDo.setTimestamp(nowAsISO);
        */

        currentUnixTime = System.currentTimeMillis() / 1000L;
        truthDo.setTimestamp(currentUnixTime);

        new Thread(new Runnable() {
            @Override
            public void run() {
                dynamoDBMapper.save(truthDo);

                Log.d("table object", truthDo.toString());
                // Item saved
            }
        }).start();
    }


    // functions for beyond verbal

    public void analyzeRecording() {
        HttpActivity httpa = new HttpActivity();

        getToken();
        responseHolder = httpa.doPost(RECORDING_URL + "start", access_token, getEntityForUpstream());

        if (responseHolder.content != null) {
            recordingid = getRecordingid(responseHolder.content);
            //goStream();
        }
        //responseHolder is the data from analyzed speech
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }


    private void requestWritePermission(int code) {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // Display a SnackBar with a button to request the missing permission.
            Snackbar.make(mLayout, "Camera access is required to display the camera preview.",
                    Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{android.Manifest.permission.CAMERA},
                            112);
                }
            }).show();

        } else {
            /*
            Snackbar.make(mLayout,
                    "Permission is not available. Requesting write permission.",
                    Snackbar.LENGTH_SHORT).show();
                    */
            // Request the permission. The result will be received in onRequestPermissionResult().
            if (code == 112)
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                                , android.Manifest.permission.RECORD_AUDIO},
                        code);
            else
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.RECORD_AUDIO},
                        code);
        }
    }


    public void checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO)
                        == PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.INTERNET)
                        == PackageManager.PERMISSION_GRANTED) {


        } else {
            Toast.makeText(getBaseContext(), "Need permission", Toast.LENGTH_LONG).show();
            requestWritePermission(112);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 112 || requestCode == 101) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];
                //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                Toast.makeText(getApplicationContext(), permission, Toast.LENGTH_LONG).show();
                if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        //授权成功后的逻辑

                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 112);
                        }
                    }
                }

                if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        //授权成功后的逻辑

                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 111);
                        }
                    }
                }

                if (permission.equals(Manifest.permission.RECORD_AUDIO)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        //授权成功后的逻辑
                        Toast.makeText(getApplicationContext(), "got it", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), "trying it", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, 101);
                        }
                    }
                }
            }
        }
    }


    private ResponseHolder postByAction(int buttonId) {
        //System.setProperty("http.proxyHost", "127.0.0.1");
        //System.setProperty("http.proxyPort", "8888");

        HttpActivity httpa = new HttpActivity();

        /*switch (buttonId) {
            case R.id.get_upstream_button:
                getToken();
                responseHolder = httpa.doPost(RECORDING_URL + "start", access_token, getEntityForUpstream());

                if (responseHolder.content != null){
                    recordingid = getRecordingid(responseHolder.content);
                    goStream();
                }
                break;
            case R.id.send_file_button:
                getToken();
                HttpEntity entity = getEntityForUpstream();
                responseHolder = httpa.doPost(RECORDING_URL + "start", access_token, entity);

                if (responseHolder.content != null){
                    recordingid = getRecordingid(responseHolder.content);
                    responseHolder = httpa.doPost(RECORDING_URL + recordingid, access_token, getEntityForSendFile());
                }
        }*/
        return responseHolder;
    }

    public void goStream() {

        Thread stream = new Thread(new Runnable() {
            @Override
            public void run() {

                HttpActivity httpac = new HttpActivity();
                final ResponseHolder hol = httpac.doPost(RECORDING_URL + recordingid, access_token, getEntityForSendFile());

                Log.d("Check", "\nFull analysis--->\n" + hol.content);
                // here we are parsing our json response into data values
                String out = hol.content;
                JSONObject js;
                try {
                    js = new JSONObject(out);
                    String id = js.getString("recordingId");
                    JSONObject r = js.getJSONObject("result");
                    JSONArray a = r.getJSONArray("analysisSegments");
                    JSONObject result = a.getJSONObject(1);
                    JSONObject analysis = result.getJSONObject("analysis");
                    JSONObject temper = analysis.getJSONObject("Temper");
                    JSONObject valence = analysis.getJSONObject("Valence");
                    JSONObject arousal = analysis.getJSONObject("Arousal");
                    double temperValue = temper.getDouble("Value");
                    String temperGroup = temper.getString("Group");
                    double temperScore = temper.getDouble("Score");
                    double valenceValue = valence.getDouble("Value");
                    String valenceGroup = valence.getString("Group");
                    double valenceScore = valence.getDouble("Score");
                    double arousalValue = arousal.getDouble("Value");
                    String arousalGroup = arousal.getString("Group");
                    double arousalScore = arousal.getDouble("Score");

                    // this is putting our data into a db
                    final BeyondVerbalDO vbdo = new BeyondVerbalDO();


                    vbdo.setRecordingID(id);

                    vbdo.setTempervalue(temperValue);
                    vbdo.setTemperscore(temperScore);
                    vbdo.setTempergroup(temperGroup);
                    vbdo.setValencevalue(valenceValue);
                    vbdo.setValenceScore(valenceScore);
                    vbdo.setValencegroup(valenceGroup);
                    vbdo.setArousalvalue(arousalValue);
                    vbdo.setArousalscore(arousalScore);
                    vbdo.setArousalgroup(arousalGroup);
                    vbdo.setphoneID(Settings.Secure.getString(MainActivity.this.getContentResolver(),
                            Settings.Secure.ANDROID_ID));
                    // vbdo.setUsername(value)
                    /*
                    TimeZone tz = TimeZone.getTimeZone("UTC");
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
                    df.setTimeZone(tz);
                    String nowAsISO = df.format(new Date());
                    */
                    vbdo.setTimestamp(currentUnixTime);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            dynamoDBMapper.save(vbdo);

                            Log.d("table object", vbdo.toString());
                            // Item saved
                        }
                    }).start();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        stream.start();
        //****
        // When post is sended file anylize file parts (Asyncronic send requests for analysis with FromMs milisecond from start file )
        // **/
        //analyze();
    }

    private void initViews() {

        //upstreamButton = initButton(R.id.get_upstream_button);
        //sendFileButton = initButton(R.id.send_file_button);
        recordButton = initButton(R.id.record_switch_button);
        //statusContent = initTextView(R.id.status_content_textView);

        //upstreamButton.setOnClickListener(this);
        //sendFileButton.setOnClickListener(this);
        //sendFileButton.setEnabled(false);
    }

    private Button initButton(int buttonId) {
        return (Button) findViewById(buttonId);
    }

    private SeekBar initSeekBar(int progressId) {
        return (SeekBar) findViewById(progressId);
    }

    private TextView initTextView(int textViewId) {
        return (TextView) findViewById(textViewId);
    }


    private long FromMs = 0;

    public void analyze() {

        final Timer myTimer = new Timer();
        long delay = 0;
        long period = 5000;
        //final Handler uiHandler = new Handler();

        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                HttpActivity httpac = new HttpActivity();
                try {

                    final ResponseHolder hol = httpac.doGet(RECORDING_URL + recordingid + "/analysis?fromMs=" + FromMs, access_token);

                    String status = getJName(hol.content, "status");
                    String sesionStatus = getsesionStatus(hol.content);
                    String f = getDuration(hol.content);
                    if (status.equals("success")) {
                        FromMs = Long.parseLong(f.replace(".0", ""));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, delay, period);

    }

    private String getsesionStatus(String response) {
        if (response == null)
            return null;
        try {
            JSONObject json = new JSONObject(response);
            String duration = json.getJSONObject("result").getString("sessionStatus");

            return duration;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getDuration(String response) {
        if (response == null)
            return null;
        try {
            JSONObject json = new JSONObject(response);
            String duration = json.getJSONObject("result").getString("duration");

            return duration;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getJName(String response, String name) {
        if (response == null)
            return null;
        try {
            JSONObject json = new JSONObject(response);
            String recordingid = json.getString(name);

            return recordingid;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }

    private String getRecordingid(String response) {
        if (response == null)
            return null;
        try {
            JSONObject json = new JSONObject(response);
            String recordingid = json.getString("recordingId");

            return recordingid;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    private class ServerConnection extends AsyncTask<Integer, Void, ResponseHolder> {

        //ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("please wait...");
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected ResponseHolder doInBackground(Integer... params) {
            int buttonId = params[0];
            responseHolder.actionId = buttonId;
            return postByAction(buttonId);
        }

        @Override
        protected void onPostExecute(ResponseHolder responseHolder) {
            statusContent.setText(responseHolder.responseString);
            if (responseHolder.content != null) {
                //setButtonByAction(responseHolder.actionId);
                CharSequence text = responseContentTextView.getText();
                responseContentTextView.setText(text + responseHolder.content);
            }

            progressDialog.dismiss();
            super.onPostExecute(responseHolder);
        }
    }

    private HttpEntity getEntityForSendFile() {

        // Fetches file from local resources.


        File f = new File(basePath + "/voice.wav");
        //Toast.makeText(this, "sending "+f.length(), Toast.LENGTH_LONG).show();

        BasicHttpEntity reqEntity = null;

        try {
            //DataInputStream dis = new DataInputStream(raw);
            FileInputStream raw = new FileInputStream(f);
            //if(raw.equals(rawR))
            reqEntity = new BasicHttpEntity();
            reqEntity.setContent(raw);
            //Toast.makeText(this, hh+"", Toast.LENGTH_LONG).show();
            //recordButton.setText(f.length()+"");
            //reqEntity = new InputStreamEntity(raw, f.length());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return reqEntity;

    }

    /**
     * @return the configuration data for get upstream url
     */
    private HttpEntity getEntityForUpstream() {
        StringEntity se = null;
        try {
            se = new StringEntity(getConfigData());
            se.setContentType("application/json; charset=UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return se;
    }

    private void getToken() {
        if (access_token != null)
            return;
        HttpActivity httpa = new HttpActivity();
        String jsonToken = httpa.doPost(Auth_URL, null, getEntityForAccessToken()).content;
        if (jsonToken == null)
            return;
        JSONObject jsonObject;
        Header header = null;
        try {
            jsonObject = new JSONObject(jsonToken);
            header = new BasicHeader("Authorization",
                    jsonObject.getString("token_type") + " " + jsonObject.getString("access_token"));
            Log.i("header", header.getName() + "  " + header.getValue());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        access_token = header;
    }

    private HttpEntity getEntityForAccessToken() {

        String body = String.format("apikey=%s&grant_type=%s", APIKey, "client_credentials");

        StringEntity se = null;
        try {
            se = new StringEntity(body);
            se.setContentType("Content-Type:application/x-www-form-urlencoded");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return se;

    }

    private String getConfigData() {
        try {
            // Instantiate a JSON Object and fill with Configuration Data
            // (Currently set to Auto Config)
            JSONObject inner_json = new JSONObject();
            inner_json.put("type", "WAV");
            inner_json.put("channels", 1);
            inner_json.put("sample_rate", 0);
            inner_json.put("bits_per_sample", 0);
            inner_json.put("auto_detect", true);
            JSONObject json = new JSONObject();
            json.put("data_format", inner_json);

            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onClick(View v) {

        new ServerConnection().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, v.getId());
    }


    // everything below is for face detection

    @Override
    protected void onResume() {
        super.onResume();
        if (isSDKStarted) {
            startDetector();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopDetector();
    }

    void startDetector() {
        if (!detector.isRunning()) {
            detector.start();
        }
    }

    void stopDetector() {
        if (detector.isRunning()) {
            detector.stop();
        }
    }

    void switchCamera(CameraDetector.CameraType type) {
        detector.setCameraType(type);
    }

    //@Override
    public void onImageResults(List<Face> list, Frame frame, float v) {
        if (list == null)
            return;
        if (list.size() == 0) {
            // do nothing

        } else {
            Face face = list.get(0);
            faceDetect = true;
            smileface = face.expressions.getSmile();

            joyface = face.emotions.getJoy();
            angerface = face.emotions.getAnger();
            disgustface = face.emotions.getDisgust();
            contemptface = face.emotions.getContempt();
            sadnessface = face.emotions.getSadness();
            fearface = face.emotions.getFear();
            surpriseface = face.emotions.getSurprise();


            /*
            switch (face.appearance.getAge()) {
                case AGE_UNKNOWN:
                    ageTextView.setText("");
                    break;
                case AGE_UNDER_18:
                    ageTextView.setText(R.string.age_under_18);
                    break;
                case AGE_18_24:
                    ageTextView.setText(R.string.age_18_24);
                    break;
                case AGE_25_34:
                    ageTextView.setText(R.string.age_25_34);
                    break;
                case AGE_35_44:
                    ageTextView.setText(R.string.age_35_44);
                    break;
                case AGE_45_54:
                    ageTextView.setText(R.string.age_45_54);
                    break;
                case AGE_55_64:
                    ageTextView.setText(R.string.age_55_64);
                    break;
                case AGE_65_PLUS:
                    ageTextView.setText(R.string.age_over_64);
                    break;
            }

            switch (face.appearance.getEthnicity()) {
                case UNKNOWN:
                    ethnicityTextView.setText("");
                    break;
                case CAUCASIAN:
                    ethnicityTextView.setText("Ethnicity: Caucasian");
                    break;
                case BLACK_AFRICAN:
                    ethnicityTextView.setText("Ethnicity: Black African");
                    break;
                case EAST_ASIAN:
                    ethnicityTextView.setText("Ethnicity: East Asian");
                    break;
                case SOUTH_ASIAN:
                    ethnicityTextView.setText("Ethnicity: South Asian");
                    break;
                case HISPANIC:
                    ethnicityTextView.setText("Ethnicity: Hispanic");
                    break;
            }*/

        }
    }

    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void onCameraSizeSelected(int width, int height, Frame.ROTATE rotate) {
        if (rotate == Frame.ROTATE.BY_90_CCW || rotate == Frame.ROTATE.BY_90_CW) {
            previewWidth = height;
            previewHeight = width;
        } else {
            previewHeight = height;
            previewWidth = width;
        }
        cameraPreview.requestLayout();
    }

    public void createFaceEmotion(String id) {
        final FaceRecognitionDO fdo = new FaceRecognitionDO();
        fdo.setAnger((double) angerface);
        fdo.setFear((double) fearface);
        fdo.setJoy((double) joyface);
        fdo.setContempt((double) contemptface);
        fdo.setSadness((double) sadnessface);
        fdo.setSmile((double) smileface);
        fdo.setSurprise((double) surpriseface);
        fdo.setphoneID(Settings.Secure.getString(MainActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID));
        //fdo.((double)disgustface);
        fdo.setUserId(id);

        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        fdo.setTimestamp(currentUnixTime);

        new Thread(new Runnable() {
            @Override
            public void run() {
                dynamoDBMapper.save(fdo);

                Log.d("Face table object", fdo.toString());
                // Item saved
            }
        }).start();
    }
}
