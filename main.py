# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import boto3

s3 = boto3.client('s3')
data_range = 10
# Open all the relevant files
java = open("2018-09-13-EEG.csv", "r")
eegraw = open("foutRaw.csv", "r")
wristBVP = open("foutBVP.csv", "r")
wristGSR = open("foutGSR.csv", "r")
wristTMP = open("foutTMP.csv", "r")




# Returns a list of rows from a given csv file
def get_entries(file):
    entry_list = []
    for x in file.readlines():
        entry_list.append(x.split(','))
    return entry_list


# Collect all rows into rows
java_list = get_entries(java)
for x in range(len(java_list)):
    java_list[x][0] = int(java_list[x][0]) / 1000
eeg_list = get_entries(eegraw)
tmp_list = get_entries(wristTMP)
bvp_list = get_entries(wristBVP)
gsr_list = get_entries(wristGSR)
# Close files because we're done using them
java.close()
eegraw.close()
wristBVP.close()
wristGSR.close()
wristTMP.close()

# for i in gsr_list:
#     del i[0]
#     if i[0] != 'gsr':
#         try:
#             i[1] = float(i[1])
#         except IndexError:
#             pass
# for i in tmp_list:
#     del i[0]
#     print(i)
#     if i[0] != 'tmp':
#         try:
#             i[1] = float(i[1])
#         except IndexError:
#             pass
# for i in bvp_list:
#     del i[0]
#     if i[0] != 'bvp':
#         try:
#             i[1] = float(i[1])
#         except IndexError:
#             pass

for i in range(0,len(gsr_list)-1):
    if 'gsr' not in gsr_list[i]:
        del gsr_list[i][0]
        gsr_list[i][0] = float(gsr_list[i][0])
    else:
        del gsr_list[i]

for i in range(0,len(bvp_list)-1):
    if 'bvp' not in bvp_list[i]:
        del bvp_list[i][0]
        bvp_list[i][0] = float(bvp_list[i][0])
    else:
        del bvp_list[i]

for i in range(0,len(tmp_list)-1):
    if 'tmp' not in tmp_list[i]:
        del tmp_list[i][0]
        tmp_list[i][0] = float(tmp_list[i][0])
    else:
        del tmp_list[i]

# In the java csv, there are 78 entries, the first is the timecode and the last is an emotion, if an emotion is
# not registered the list element is empty
# Move through list again and get timestamps of events
timestamps = []
emotion = []
for i, entry in enumerate(java_list):
    if entry[77] != "\n":
        # Need to clean timestamp for other comparisons
        timestamps.append(int(entry[0]))
        emotion.append(entry[77].rstrip())
    java_list[i].pop()


# Given a list of times, searches each first index of row (which should have timestamp)
# returns a list of only the rows which are closest to the times
def data_timing(row_list, times, outside):
    index = 0
    found = False
    out_list = []
    for x, time in enumerate(times):
        for sub_time in range(-outside, outside):
            # print(sub_time)
            while not found:
                try:
                    if time + sub_time < float(row_list[index][0]):
                        found = True
                        out_list.append(row_list[index - 1])
                    else:
                        index += 1
                except IndexError:
                    break
            found = False
    return out_list


# Combiner, takes a filename and list of appropriate(same length files)
def combiner(filename, things):
    file = open(filename, "w")
    # print(things)
    for index in range(len(things[0])):
        for list in things:
            temp_loc = min(timestamps, key=lambda y: abs(y - float(list[0][0])))
            ind = timestamps.index(temp_loc)
            file.write(emotion[ind])
            for item in list[index]:
                file.write("," + str(item))

    #s3.meta.client.upload_file(filename, 'store-transform-data', file)
    file.close()


# make few files and combine
eeg_data = []
eeg_data.append(data_timing(java_list, timestamps, data_range))
eeg_data.append(data_timing(eeg_list, timestamps, data_range))
combiner("specific\\eeg.csv", eeg_data)

s3.upload_file("specific\\eeg.csv", "store-transform-data", "specific_eeg.csv")

watch_data = []
watch_data.append(data_timing(bvp_list, timestamps, data_range))
watch_data.append(data_timing(gsr_list, timestamps, data_range))
watch_data.append(data_timing(tmp_list, timestamps, data_range))
combiner("specific\\watch.csv", watch_data)

s3.upload_file("specific\\watch.csv", "store-transform-data", "specific_watch.csv")
# print(bvp_list[0],tmp_list[0],gsr_list[0])

# wrist = open("specific/wristband.csv")


def range_returner(filename, row_list, times, emotions, outside):
    index = 0
    tmp_file = open(filename, "w")
    for x, spec_time in enumerate(times):
        chosen_time = float(spec_time)
        while float(row_list[index][0]) <= chosen_time + outside:
            index += 1
            if index > len(row_list):
                return
            if float(row_list[index][0]) >= chosen_time - outside:
                tmp_file.write(emotions[x] + "," + row_list[index][-1].rstrip() + "\n")
    tmp_file.close()

# transform existing files
# range_returner("range\\javaeeg.csv", java_list, timestamps, emotion, data_range)
# range_returner("range\\newRaw.csv", eeg_list, timestamps, emotion, data_range)
# range_returner("range\\newBVP.csv", bvp_list, timestamps, emotion, data_range)
# range_returner("range\\newGSR.csv", gsr_list, timestamps, emotion, data_range)
# range_returner("range\\newTMP.csv", tmp_list, timestamps, emotion, data_range)
